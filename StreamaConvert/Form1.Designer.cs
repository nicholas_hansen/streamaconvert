﻿namespace StreamaConvert
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonChoose = new System.Windows.Forms.Button();
            this.buttonConvert = new System.Windows.Forms.Button();
            this.labelStatusConvert = new System.Windows.Forms.Label();
            this.buttonFFMPEG = new System.Windows.Forms.Button();
            this.labelStatusChoose = new System.Windows.Forms.Label();
            this.labelStatusFFMPEG = new System.Windows.Forms.Label();
            this.listBoxConversion = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // buttonChoose
            // 
            this.buttonChoose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonChoose.Location = new System.Drawing.Point(344, 11);
            this.buttonChoose.Margin = new System.Windows.Forms.Padding(2);
            this.buttonChoose.Name = "buttonChoose";
            this.buttonChoose.Size = new System.Drawing.Size(56, 61);
            this.buttonChoose.TabIndex = 0;
            this.buttonChoose.Text = "Choose";
            this.buttonChoose.UseVisualStyleBackColor = true;
            this.buttonChoose.Click += new System.EventHandler(this.buttonChoose_Click);
            // 
            // buttonConvert
            // 
            this.buttonConvert.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonConvert.Location = new System.Drawing.Point(344, 77);
            this.buttonConvert.Margin = new System.Windows.Forms.Padding(2);
            this.buttonConvert.Name = "buttonConvert";
            this.buttonConvert.Size = new System.Drawing.Size(56, 61);
            this.buttonConvert.TabIndex = 1;
            this.buttonConvert.Text = "Convert";
            this.buttonConvert.UseVisualStyleBackColor = true;
            this.buttonConvert.Click += new System.EventHandler(this.buttonConvert_Click);
            // 
            // labelStatusConvert
            // 
            this.labelStatusConvert.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelStatusConvert.Location = new System.Drawing.Point(405, 101);
            this.labelStatusConvert.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelStatusConvert.Name = "labelStatusConvert";
            this.labelStatusConvert.Size = new System.Drawing.Size(306, 20);
            this.labelStatusConvert.TabIndex = 3;
            this.labelStatusConvert.Text = "Create Streama compatible files in the \"Converted\" directory.";
            // 
            // buttonFFMPEG
            // 
            this.buttonFFMPEG.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonFFMPEG.Location = new System.Drawing.Point(344, 142);
            this.buttonFFMPEG.Margin = new System.Windows.Forms.Padding(2);
            this.buttonFFMPEG.Name = "buttonFFMPEG";
            this.buttonFFMPEG.Size = new System.Drawing.Size(79, 61);
            this.buttonFFMPEG.TabIndex = 4;
            this.buttonFFMPEG.Text = "FFMPEG Location";
            this.buttonFFMPEG.UseVisualStyleBackColor = true;
            this.buttonFFMPEG.Click += new System.EventHandler(this.buttonFFMPEG_Click);
            // 
            // labelStatusChoose
            // 
            this.labelStatusChoose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelStatusChoose.AutoSize = true;
            this.labelStatusChoose.Location = new System.Drawing.Point(405, 34);
            this.labelStatusChoose.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelStatusChoose.Name = "labelStatusChoose";
            this.labelStatusChoose.Size = new System.Drawing.Size(118, 13);
            this.labelStatusChoose.TabIndex = 5;
            this.labelStatusChoose.Text = "Choose files to convert.";
            // 
            // labelStatusFFMPEG
            // 
            this.labelStatusFFMPEG.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.labelStatusFFMPEG.AutoSize = true;
            this.labelStatusFFMPEG.Location = new System.Drawing.Point(427, 166);
            this.labelStatusFFMPEG.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.labelStatusFFMPEG.Name = "labelStatusFFMPEG";
            this.labelStatusFFMPEG.Size = new System.Drawing.Size(163, 13);
            this.labelStatusFFMPEG.TabIndex = 6;
            this.labelStatusFFMPEG.Text = "Choose location of \"ffmpeg.exe\".";
            // 
            // listBoxConversion
            // 
            this.listBoxConversion.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.listBoxConversion.FormattingEnabled = true;
            this.listBoxConversion.Location = new System.Drawing.Point(12, 12);
            this.listBoxConversion.Name = "listBoxConversion";
            this.listBoxConversion.Size = new System.Drawing.Size(327, 199);
            this.listBoxConversion.TabIndex = 7;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(705, 217);
            this.Controls.Add(this.listBoxConversion);
            this.Controls.Add(this.labelStatusFFMPEG);
            this.Controls.Add(this.labelStatusChoose);
            this.Controls.Add(this.buttonFFMPEG);
            this.Controls.Add(this.labelStatusConvert);
            this.Controls.Add(this.buttonConvert);
            this.Controls.Add(this.buttonChoose);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.Text = "Streama Convert";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button buttonChoose;
        private System.Windows.Forms.Button buttonConvert;
        private System.Windows.Forms.Label labelStatusConvert;
        private System.Windows.Forms.Button buttonFFMPEG;
        private System.Windows.Forms.Label labelStatusChoose;
        private System.Windows.Forms.Label labelStatusFFMPEG;
        private System.Windows.Forms.ListBox listBoxConversion;
    }
}

