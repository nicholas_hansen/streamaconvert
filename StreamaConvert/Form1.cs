﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StreamaConvert
{
    public partial class Form1 : Form
    {
        string ffmpegLocation = "";
        //string videoToConvert = "";
        Queue<string> videosToConvert = new Queue<string>();

        string convertedFolderName = "Converted";

        int maxNumProcesses = 2;

        int numItems = 0;

        public Form1()
        {
            InitializeComponent();
        }

        private void buttonFFMPEG_Click(object sender, EventArgs e)
        {
            var fileDialog = new OpenFileDialog();
            fileDialog.Title = "Choose ffmpeg.exe";

            if (fileDialog.ShowDialog() == DialogResult.OK)
            {
                ffmpegLocation = fileDialog.FileName;
                Properties.Settings.Default.ffmpegexe = ffmpegLocation;
                Properties.Settings.Default.Save();

                labelStatusFFMPEG.Text = ffmpegLocation;
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            ffmpegLocation = Properties.Settings.Default.ffmpegexe;
            if ("" == ffmpegLocation)
            {
                labelStatusFFMPEG.Text = "Choose location of \"ffmpeg.exe\".";
            }
            else
            {
                labelStatusFFMPEG.Text = ffmpegLocation;
            }
        }

        private void buttonChoose_Click(object sender, EventArgs e)
        {
            // Choose files to convert.
            var openDialog = new OpenFileDialog();
            openDialog.Title = "Choose videos to convert";
            openDialog.Multiselect = true;
            if (openDialog.ShowDialog() == DialogResult.OK)
            {
                AddToQueue(openDialog.FileNames);
                //labelStatusChoose.Text = videoToConvert;
            }
        }

        private void AddToQueue(string[] fileNames)
        {
            foreach (string filename in fileNames)
            {
                videosToConvert.Enqueue(filename);
            }
            SyncQueue();
        }

        private void SyncQueue()
        {
            // do we need to switch threads?
            if (InvokeRequired)
            {
                // slightly different now, as we dont need params
                // we can just use MethodInvoker
                MethodInvoker method = new MethodInvoker(SyncQueue);
                Invoke(method);
                return;
            }

            listBoxConversion.BeginUpdate();
            listBoxConversion.Items.Clear();
            foreach (string file in videosToConvert)
            {
                listBoxConversion.Items.Add(file);
            }
            listBoxConversion.EndUpdate();
        }

        private void buttonConvert_Click(object sender, EventArgs e)
        {
            if ("" == ffmpegLocation)
            {
                MessageBox.Show("Choose location of ffmpeg.exe before converting.");
                return;
            }

            //if ("" == videoToConvert)
            if (videosToConvert.Count == 0)
            {
                MessageBox.Show("Choose videos to convert.");
                return;
            }

            buttonConvert.Enabled = false;
            numItems = videosToConvert.Count;
            //ProcessVideoQueue();
            //workerVideoConverter.RunWorkerAsync();
            for (int i = 0; i < maxNumProcesses; ++i)
            {
                StartNewVideoProcess();
            }

        }

        //private bool ProcessVideo(string filepath)
        private void ProcessVideo(string filepath)
        {
            // Take a file from the queue.
            //if (videosToConvert.Count == 0) return false;
            //string filepath = videosToConvert.Dequeue();

            FileInfo videoFile = new FileInfo(filepath);
            if (!videoFile.Exists)
            {
                //MessageBox.Show("Chosen file does not exist.");
                // TODO: Attempt to process next video.
                return;// false;
            }

            // Make sure the Converted folder exists.
            DirectoryInfo outputDir = new DirectoryInfo(videoFile.Directory.FullName + Path.DirectorySeparatorChar + convertedFolderName);
            if (!outputDir.Exists)
            {
                outputDir.Create();
            }
            if (!outputDir.Exists)
            {
                return;// false;
            }

            // Launch a process to convert the video.
            // Expected formet is:
            // ffmpeg -i input.mov -vcodec h264 -acodec aac -strict -2 output.mp4

            //string outputFileName = videoFile.Directory.FullName + Path.DirectorySeparatorChar + Path.GetFileNameWithoutExtension(videoFile.Name) + "c.mp4";
            string outputFileName = outputDir.FullName + Path.DirectorySeparatorChar + Path.GetFileNameWithoutExtension(videoFile.Name) + ".mp4";

            Process process = new Process();
            // Configure the process using the StartInfo properties.
            process.StartInfo.FileName = ffmpegLocation;
            process.StartInfo.Arguments = "-i \"" + videoFile.FullName + "\" " + "-vcodec h264 -acodec aac -strict -2 " + "\"" + outputFileName + "\"";
            process.StartInfo.WindowStyle = ProcessWindowStyle.Normal;

            process.EnableRaisingEvents = true;

            // Let the end event get fired instead.
            process.Exited += Process_Exited;

            process.Start();
            //process.WaitForExit();
            // Waits here for the process to exit.



            return;// true;
        }

        private void Process_Exited(object sender, EventArgs e)
        {
            int numFinished = numItems - videosToConvert.Count;
            int progress = (int)(numFinished * 100.0 / numItems);
            //workerVideoConverter.ReportProgress(progress, filepath);

            StartNewVideoProcess();
        }

        bool cancelProcessing = false;

        private void StartNewVideoProcess()
        {
            if (!cancelProcessing && videosToConvert.Count != 0)
            {
                string filepath = videosToConvert.Dequeue();

                SyncQueue();

                ProcessVideo(filepath);
            }
        }

        //private void workerVideoConverter_DoWork(object sender, DoWorkEventArgs e)
        //{
        //    workerVideoConverter.ReportProgress(0);

        //    int numItems = videosToConvert.Count;
        //    int numFinished = 0;
        //    // Dequeue a file from the list and convert it.
        //    while (!cancelProcessing && videosToConvert.Count != 0)
        //    {
        //        string filepath = videosToConvert.Dequeue();
        //        bool result = ProcessVideo(filepath);

        //        if (!result)
        //        {
        //            MessageBox.Show("Error occured while processing video.");
        //            return;
        //        }

        //        numFinished = numItems - videosToConvert.Count;
        //        int progress = (int)(numFinished * 100.0 / numItems);
        //        workerVideoConverter.ReportProgress(progress, filepath);
        //    }

        //    workerVideoConverter.ReportProgress(100);
        //}

        //private void workerVideoConverter_ProgressChanged(object sender, ProgressChangedEventArgs e)
        //{
        //    conversionProgress.Value = e.ProgressPercentage;

        //    if (null != e.UserState)
        //    {
        //        listBoxConversion.BeginUpdate();
        //        listBoxConversion.Items.Remove((string)e.UserState);
        //        listBoxConversion.EndUpdate();
        //    }

        //    //SyncQueue();
        //}

        //private void workerVideoConverter_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        //{
        //    // Reenable the button.
        //    buttonConvert.Enabled = true;
        //}
    }
}
